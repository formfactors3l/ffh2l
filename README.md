[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.11046426.svg)](https://doi.org/10.5281/zenodo.11046426)
# FFh2l

**FFh2l** is a Fortran library that evaluates the third-order corrections to the QCD form factors for heavy-to-light transition (e.g. $`b \to u`$).
It uses interpolated grids for the evaluation of the form factors in the range $`-75 < s/m^2 < 0.9375`$,
while it uses special series expansion around $`s= 1`$ and $`s=-\infty`$.

It is based on the results presented in:

M. Fael, T. Huber, F. Lange, J. Müller, K. Schönwald, M. Steinhauser     
Heavy-to-light form factors to three loops           
hep-ph/2406.08182

## Installation
The library can be downloaded from gitlab

```
$ git clone https://gitlab.com/formfactors3l/ffh2l.git
```

The library contains in the main directory `ffh2l` the following files and directories:
- `makefile`: the makefile to produce the library
- `src`: source directory, containing the main source files of ffh2l and further source files in the subdirectories
	- `grids`: grid directory, containing the modules for the evaluation of the form factors via Chebyshev grids
	- `series`: series directory, containing the modules for the evaluation of the form factors around $`s=1`$
- `examples`: directory containing example programs for the library usage
- `build`: build directory, where make puts all necessary files for the creation of the library
- `modules`: directory for fortran module files

To compile the package, a Fortran compiler is required. 
Execute the following commands to compile the library

```
$ ./configure
$ make
```
The command make will generate the static library `libffh2l.a` in
the directory `ffh2l` which can be linked to the user’s program.
We provide also a Mathematica interface which can be complied with
```
$ make mathlink
```
The example programs can be compiled with
```
$ make examples
```



## General usage instructions

In order to use **FFhl2** in a Fortran program, the corresponding modules located in the directory `modules` 
have to be loaded by including the line

```
use ffh2l
```

in the preamble of the respective code. The library `libffh2l.a` has to be supplied to the linker.
This gives access to the public functions and subroutines described in the following. 
The names of all these routines start with the suffix `ffh2l_`.

## UV renormalized form factors at three loops

The functions `ffh2l_type`, where `type = veF1, veF2, veF3, axF1, axF2, axF3, scF1, psF1, teF1, teF2, teF3, teF4, scF1OS, psF1OS`, 
return the contributions to the UV renormalized form factors at three loops.
The result is the third-order correction in the expansion parameter $`\alpha_s^{(n_l)}/(4\pi)`$,
the strong coupling constant renormalized in the $`\overline{\mathrm{MS}}`$ scheme
with the renormalization scale equal to the mass of the heavy quark: $`\mu_s=m`$.

Note that the form factors `scF1` and `psF1` have been implemented using the 
renormalization constants for the currents $`Z_s=Z_p=Z_m^{\overline{\mathrm{MS}}}`$.
The user can utilize `scF1OS` and `psF1OS` to obtain results for the 
scalar and pseudoscalar form factors with $`Z_s=Z_p=Z=Z_m^{\mathrm{OS}}`$ for the
current renormalization.

The functions return a `double complex` and have the following two inputs:
```
double complex function ffhl2_veF1 (s,eporder)
    double precision, intent(in) :: s
    integer, intent(in) :: eporder
```
The variable `s` is the value of the momentum transfer normalized w.r.t. the quark mass.
The order in the dimensional regulator $`\epsilon; = (4-d)/2`$ is set by the integer `eporder`.
Only the values `eporder=-6,...,0` are valid.

## Finite remainder after IR subtraction

The functions `ffh2l_type_fin`, where `type = veF1, veF2, veF3, axF1, axF2, axF3, scF1, psF1, teF1, teF2, teF3, teF4, scF1OS, psF1OS`, 
return the contributions to the UV renormalized and IR subtracted form factors at three loops.
The result is the third-order correction in the expansion parameter $`\alpha_s^{(n_l)}/(4\pi)`$,
the strong coupling constant renormalized in the $`\overline{\mathrm{MS}}`$ scheme
with the renormalization scale equal to the mass of the heavy quark: $`\mu_s=m`$.

Also in this case, the routines with `scF1` and `psF1` correspond to the form factors 
renormalized with $`Z_s=Z_p=Z_m^{\overline{\mathrm{MS}}}`$. We provide additionally
two routines identified by `scF1OS` and `psF1OS` for the finite remainder of the 
scalar and pseudoscalar form factors with $`Z_s=Z_p=Z_m^{\mathrm{OS}}`$.

The functions return a `double complex` and have the following input:
```
double complex function ffhl2_veF1_fin (s)
    double precision, intent(in) :: s
```
The variable `s` is the value of the momentum transfer normalized w.r.t. the quark mass.

## Options

The number of massive and massless quarks can be set via the following subroutine:

```
integer :: nl = 1
integer :: nh = 1

call ffh2l_set_nl(nl) 
call ffh2l_set_nh(nh)
```

In the current implementation, the numerical values of the Casimir are hard coded for QCD
in the file `ffh2l_global.F90`. We have $`C_F=4/3, C_A=3, T_F=1/2`$. 

## Mathematica 
The Mathematica interface must be loaded first:
```
In[] := Install["PATH/ffh2l"]
```
where `PATH/ffh2l` is the location where the mathlink executable `ffh2l` is saved.
The UV renormalized form factors in QCD are evaluated with one of the following: 
`FFh2lveF1, FFh2lveF2, FFh2lveF3, FFh2laxF1, FFh2laxF2, FFh2laxF3, FFh2lscF1, FFh2lpsF1, FFh2lteF1, FFh2lteF2, FFh2lteF3, FFh2lteF4, FFh2lscF1OS, FFh2lpsF1OS`
```
In[] := s = 3/10;
In[] := eporder = 0;
In[] := FFh2lveF1[s,eporder]
Out[]:= 2439.87
```
The finite remainder of the form factors after UV renormalization in IR subtraction
are evaluated with one of the following: 
`FFh2lveF1Fin, FFh2lveF2Fin, FFh2lveF3Fin, FFh2laxF1Fin, FFh2laxF2Fin, FFh2laxF3Fin, FFh2lscF1Fin, FFh2lpsF1Fin, FFh2lteF1Fin, FFh2lteF2Fin, FFh2lteF3Fin, FFh2lteF4Fin, FFh2lscF1OSFin, FFh2lpsF1OSFin`
```
In[] := s = 3/10;
In[] := FFh2lveF1Fin[s]
Out[]:= -8467.54
```
The number of massless and massive quarks can be set also from Mathematica:
```
In[] := FFh2lSetNl[nl]
In[] := FFh2lSetNh[nh]
```
where `nl` and `nh` are positive integers. Their default values are `nl=4` and `nh=1`.

## Chebyshev interpolation grids

Interpolation grids are based on the Chebyshev approximation method (see [here](https://www.cec.uchile.cl/cinetica/pcordero/MC_libros/NumericalRecipesinC.pdf#page=214)).
Chebyshev polynomial is close to the minimal polynomial, which (among all polynomials of the same degree) has the smallest maximum deviation from the true function f(x).

## Final comments

After UV renormalization, some of the poles of the form factors cancel. Due to the numerical evaluation of the master integrals,
the pole cancellation happens only up to certain numerical precision (see the discussion in Sec. 6.1 of hep-ph/2207.00027). 
In the current Fortran implementation we explicitly set to zero the coefficients of certain $`1/\epsilon`$ poles to avoid possible spurious numerical instabilities.
In order to identify the poles to set to zero, we used the following approach:
In each interpolation region, we compute the form factors at certain points of `s` corresponding to the zeros of the Chebyshev polynomials. 
For each color factor and each order in $`\epsilon`$, we calculate the averaged value. If the average (in absolute value) is smaller than $`10^{-10}`$
then we set to zero the corresponding coefficient in the $`1/\epsilon`$ expansion.
