/****** QCD ******/
:Begin:
:Function:	veF1
:Pattern: 	FFh2lveF1[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	veF2
:Pattern: 	FFh2lveF2[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	veF3
:Pattern: 	FFh2lveF3[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	axF1
:Pattern: 	FFh2laxF1[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	axF2
:Pattern: 	FFh2laxF2[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	axF3
:Pattern: 	FFh2laxF3[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	scF1
:Pattern: 	FFh2lscF1[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	psF1
:Pattern: 	FFh2lpsF1[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	teF1
:Pattern: 	FFh2lteF1[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	teF2
:Pattern: 	FFh2lteF2[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	teF3
:Pattern: 	FFh2lteF3[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	teF4
:Pattern: 	FFh2lteF4[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	scF1OS
:Pattern: 	FFh2lscF1OS[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	psF1OS
:Pattern: 	FFh2lpsF1OS[s_?NumericQ, ep_Integer]
:Arguments:	{N[s], ep}
:ArgumentTypes:	{Real, Integer}
:ReturnType:	Manual
:End:

/****** QCD - Finite Remainder after IR subtraction ******/

:Begin:
:Function:	veF1Fin
:Pattern: 	FFh2lveF1Fin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	veF2Fin
:Pattern: 	FFh2lveF2Fin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	veF3Fin
:Pattern: 	FFh2lveF3Fin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	axF1Fin
:Pattern: 	FFh2laxF1Fin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	axF2Fin
:Pattern: 	FFh2laxF2Fin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	axF3Fin
:Pattern: 	FFh2laxF3Fin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	scF1Fin
:Pattern: 	FFh2lscF1Fin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	psF1Fin
:Pattern: 	FFh2lpsF1Fin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	teF1Fin
:Pattern: 	FFh2lteF1Fin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	teF2Fin
:Pattern: 	FFh2lteF2Fin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	teF3Fin
:Pattern: 	FFh2lteF3Fin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	teF4Fin
:Pattern: 	FFh2lteF4Fin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	scF1OSFin
:Pattern: 	FFh2lscF1OSFin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:

:Begin:
:Function:	psF1OSFin
:Pattern: 	FFh2lpsF1OSFin[s_?NumericQ]
:Arguments:	{N[s]}
:ArgumentTypes:	{Real}
:ReturnType:	Manual
:End:


/******* AUX FUNCTIONS ********/

:Begin:
:Function:	set_nl
:Pattern: 	FFh2lSetNl[nl_Integer]
:Arguments:	{nl}
:ArgumentTypes:	{Integer}
:ReturnType:	Manual
:End:

:Begin:
:Function:	set_nh
:Pattern: 	FFh2lSetNh[nh_Integer]
:Arguments:	{nh}
:ArgumentTypes:	{Integer}
:ReturnType:	Manual
:End:

#include "mathlink.h"
#include "complex.h"

extern complex ffh2l_vef1(double *, int *);
extern complex ffh2l_vef2(double *, int *);
extern complex ffh2l_vef3(double *, int *);
extern complex ffh2l_axf1(double *, int *);
extern complex ffh2l_axf2(double *, int *);
extern complex ffh2l_axf3(double *, int *);
extern complex ffh2l_scf1(double *, int *);
extern complex ffh2l_psf1(double *, int *);
extern complex ffh2l_tef1(double *, int *);
extern complex ffh2l_tef2(double *, int *);
extern complex ffh2l_tef3(double *, int *);
extern complex ffh2l_tef4(double *, int *);
extern complex ffh2l_scf1os(double *, int *);
extern complex ffh2l_psf1os(double *, int *);

extern complex ffh2l_vef1_fin(double *);
extern complex ffh2l_vef2_fin(double *);
extern complex ffh2l_vef3_fin(double *);
extern complex ffh2l_axf1_fin(double *);
extern complex ffh2l_axf2_fin(double *);
extern complex ffh2l_axf3_fin(double *);
extern complex ffh2l_scf1_fin(double *);
extern complex ffh2l_psf1_fin(double *);
extern complex ffh2l_tef1_fin(double *);
extern complex ffh2l_tef2_fin(double *);
extern complex ffh2l_tef3_fin(double *);
extern complex ffh2l_tef4_fin(double *);
extern complex ffh2l_scf1os_fin(double *);
extern complex ffh2l_psf1os_fin(double *);

extern void    ffh2l_set_nl(int *);
extern void    ffh2l_set_nh(int *);

/******* QCD **********/
static void veF1(double s, int ep){
	complex res;

	res = ffh2l_vef1(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void veF2(double s, int ep){
	complex res;

	res = ffh2l_vef2(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void veF3(double s, int ep){
	complex res;

	res = ffh2l_vef3(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void axF1(double s, int ep){
	complex res;

	res = ffh2l_axf1(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void axF2(double s, int ep){
	complex res;

	res = ffh2l_axf2(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void axF3(double s, int ep){
	complex res;

	res = ffh2l_axf3(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void scF1(double s, int ep){
	complex res;

	res = ffh2l_scf1(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void psF1(double s, int ep){
	complex res;

	res = ffh2l_psf1(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void teF1(double s, int ep){
	complex res;

	res = ffh2l_tef1(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void teF2(double s, int ep){
	complex res;

	res = ffh2l_tef2(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void teF3(double s, int ep){
	complex res;

	res = ffh2l_tef3(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void teF4(double s, int ep){
	complex res;

	res = ffh2l_tef4(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void scF1OS(double s, int ep){
	complex res;

	res = ffh2l_scf1os(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void psF1OS(double s, int ep){
	complex res;

	res = ffh2l_psf1os(&s,&ep);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

/******* QCD - Finite remainder **********/
static void veF1Fin(double s){
	complex res;

	res = ffh2l_vef1_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void veF2Fin(double s){
	complex res;

	res = ffh2l_vef2_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void veF3Fin(double s){
	complex res;

	res = ffh2l_vef3_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void axF1Fin(double s){
	complex res;

	res = ffh2l_axf1_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void axF2Fin(double s){
	complex res;

	res = ffh2l_axf2_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void axF3Fin(double s){
	complex res;

	res = ffh2l_axf3_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void scF1Fin(double s){
	complex res;

	res = ffh2l_scf1_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void psF1Fin(double s){
	complex res;

	res = ffh2l_psf1_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void teF1Fin(double s){
	complex res;

	res = ffh2l_tef1_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void teF2Fin(double s){
	complex res;

	res = ffh2l_tef2_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void teF3Fin(double s){
	complex res;

	res = ffh2l_tef3_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void teF4Fin(double s){
	complex res;

	res = ffh2l_tef4_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void scF1OSFin(double s){
	complex res;

	res = ffh2l_scf1os_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}

static void psF1OSFin(double s){
	complex res;

	res = ffh2l_psf1os_fin(&s);

	if( cimag(res) == 0 ) MLPutReal(stdlink, creal(res));
	else {
		MLPutFunction(stdlink, "Complex", 2);
		MLPutReal(stdlink, creal(res));
        	MLPutReal(stdlink, cimag(res));
	}
}


/********* AUX FUNCTIONS ************/
static void set_nl(int nl){
	ffh2l_set_nl(&nl);
	MLPutSymbol(stdlink, "Null"); 
  	MLEndPacket(stdlink);
}

static void set_nh(int nh){
	ffh2l_set_nh(&nh);
	MLPutSymbol(stdlink, "Null"); 
  	MLEndPacket(stdlink);
}

int main(int argc, char **argv){

	return MLMain(argc,argv);
}	

